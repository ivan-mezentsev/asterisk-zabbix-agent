#!/usr/bin/php
<?php
error_reporting(1);

if ($argv[1] == '' OR $argv[2] == '') exit("error");

exec("/usr/sbin/asterisk -rx 'sip show peer " . $argv[2] . "'|grep --text 'Addr->IP'|awk '{print $3}'", $adds);


$row = $adds[0];
$str = strpos($row, ":");
$row = substr($row, 0, $str);

//Удаляем все символы кроме цифр и точек
$adds = preg_replace('~[^0-9|.]*~is', '', $row);

if (!is_ip($adds)) {
    exit("0.00");
}


if ($argv[1] == 'rx') {
    exec("/usr/sbin/asterisk -rx 'sip show channelstats'|grep " . $adds . "|awk '{print $(NF-6)}'", $lost);
}
if ($argv[1] == 'tx') {
    exec("/usr/sbin/asterisk -rx 'sip show channelstats'|grep " . $adds . "|awk '{print $(NF-1)}'", $lost);

}


if ($lost[0] == '') {
    exit("0.00");
}

$result = preg_replace('~[^0-9|.]*~is', '', $lost[0]);

if ($result == '') {
    exit("0.00");
}

exit($result);

function is_ip($value)
{
    $check = "true";
    if (!filter_var($value, FILTER_VALIDATE_IP)) {
        $check = "false";
    }
    if ($check == "true") {
        return true;
    } else {
        return false;
    }
}
