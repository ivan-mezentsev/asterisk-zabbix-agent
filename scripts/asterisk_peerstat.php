#!/usr/bin/php
<?php
error_reporting(1);
function getStatus($item)
{
    $cmd = "/usr/sbin/asterisk -rx 'sip show peer " . $item . "'| grep -i --text 'Status'|awk '{print \$3}'";
    exec($cmd, $st);
    if ($st[0] == 'OK') {
        echo "1";
    } else {
        echo "0";
    }
}

if ($argv[1] != '') {
    getStatus($argv[1]);
} else {
    print "error";
}
?>