#!/usr/bin/php
<?php
error_reporting(1);
exec("/usr/sbin/asterisk -rx 'sip show peers'|grep -v 'sip peers'|awk '{print $1}'", $tr_list);
foreach ($tr_list as $line) {
    $str_new = explode('/', $line);
    $line = $str_new[0];
    $ar_line = null;
    $ar_line[] = $line;
    if (!is_exten($line)) {
        $trunks[] = $ar_line['0'];
    }
}
$trunks[0] = '';
$trunks = array_diff($trunks, array(''));
function getJson($items, $name)
{
    $first = 1;
    print "{\n";
    print "\t\"data\":[\n\n";
    $lastitem = end($items);
    foreach ($items as $item) {
        if ($item != $lastitem) {
            if (!$first) {
                print "\t,\n";
                $first = 0;
            }
            print "\t{\n";
            print "\t\t\"{#$name}\":\"$item\"\n";
            print "\t},\n";
        } else {
            if (!$first) {
                print "\t,\n";
                $first = 0;
            }
            print "\t{\n";
            print "\t\t\"{#$name}\":\"$item\"\n";
            print "\t}\n";
        }
    }
    print "\n\t]\n";
    print "}\n";
}

if ($argv[1] == 'trunks') {
    getJson($trunks, "TRUNKNAME");
} else {
    print "error";
}


function is_exten($value)
{
    if (preg_match('/^\+?\d+$/', $value)) {
        return true;
    } else {
        return false;
    }
}
